import fetch from 'node-fetch';
// import { Promise } from 'node-fetch'
// import {Promise} from 'es6-promise';
import config from './api-path';
require('es6-promise').polyfill();
export const path = process.env.HOST || 'localhost',
    port = process.env.PORT || 4000,
    api_url = 'https://api.themoviedb.org/3/movie'; // 'https://unnusoft.herokuapp.com/api'; //config.BASE_URL || `http://${path}:${port}/api`;

export default function apiConfig(endpoint, params, method = 'GET', body) {
    return fetch(`${api_url}/${endpoint}?api_key=fd0b40147423fed91e4c25a1454909db&language=en-US&page=1`, {
            method,
            body: JSON.stringify(body),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response => response.json()
            .then(json => ({ json, response }))
        ).then(({ json, response }) => {
            if (!response.ok) {
                return json;
            }

            return json;
        }).then(
            response => response
        ).catch(e => e);
}
