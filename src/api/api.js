import API from './api-config';

export function getInitialData(dispatch, request) {
    return API('popular').then(data => dispatch(request(data)));
}



