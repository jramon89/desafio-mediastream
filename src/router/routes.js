import Layout from '../views/Layout/Layout';
import Views from '../views';
import React from 'react';

const routes = [
    {
        component: Layout,
        routes: [
            {
                path: '/',
                exact: true,
                component: Views.Home
            },
            {
                path: '/movie/:id',
                component: Views.PostsDetails
            },
            {
                path: '/populares',
                component: Views.Popular
            },
            {
                path: '/buscar/:id',
                component: Views.Search
            },
        ]
    }
];

export default routes;
