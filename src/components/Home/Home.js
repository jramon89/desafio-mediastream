import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PostsList from '../../components/Posts/Posts';




class Home extends Component {
    static propTypes = {
        posts: PropTypes.array,
    };

    constructor(props){
        super(props);
    }

    render() {
        const {
            posts
        } = this.props;
        return(
            <div>
                <PostsList data={ posts } meta={{
                    title: '',
                    description: ''
                }}/>
            </div>
        );
    }
}

export default Home;