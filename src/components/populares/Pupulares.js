import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PostsList from '../../components/Posts/Posts';

export default
class PopularList extends Component {
    static propTypes = {
        data: PropTypes.array
    };
    constructor(props){
        super(props);
        this.state={
            items:[]
        };
    }

    render() {
        return(
            <div>
                <PostsList data={ this.props.data } meta={{
                    title: '',
                    description: ''
                }}/>
            </div>
        );
    }
}
