import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Search from '../../Common/Search/Search';

export default
class Menu extends Component {
    static propTypes = {
        onSubmit: PropTypes.func,
        items: PropTypes.object,
        value: PropTypes.string,
        dispatch: PropTypes.func,
        isScrolling: PropTypes.bool,
        posts: PropTypes.object
    };
    constructor(props) {
        super(props);
        this.state = {
            isOpenMenu: false,
            topHeightNav: null,
        };
        this.changeCategory = this.changeCategory.bind(this);
        this.openMenu = this.openMenu.bind(this);
        this.onSubmitSearch = this.onSubmitSearch.bind(this);
    }

    scroll(e, value) {
        const anchor = `#${value}`
        document.querySelector(anchor)
            .scrollIntoView({
                behavior: 'smooth'
            });
        history.pushState(null, null, anchor);
        e.preventDefault();
    }
    openMenu() {
        const { isOpenMenu } = this.state;
        this.setState({
            isOpenMenu: !isOpenMenu,
            topHeightNav: this.ofHeight.offsetHeight,
            hideItemsMenu: false,
        });
    }
    onSubmitSearch(value) {
        const { onSubmit } = this.props;
        onSubmit(value);
        this.setState({
            isOpenMenu: false
        });
    }
    render() {
        const {
            items,
            value,
            location,
            isScrolling
        } = this.props;
        const typeListMenu = location.pathname.includes('blog') ||
        location.pathname.includes('cursos') ||
        location.pathname.includes('tutoriales') ? 'menu_a' : 'menu_b';

        const isOpenMenu = this.state.isOpenMenu ? 'active' : '';
        const logo = !isScrolling ? 'logo-1.2' : 'unnusoft-logo-title-v2';

        const menuItems = items['menu_a'].map((value, index)=>{

            let type = index === 0 ? (
                <div className="">
                    <Link className="logo" to="/" onClick={() => this.setState({isOpenMenu: false})}>
                        <div className="logo-title">ONGETMOVIES</div>
                    </Link>
                    <span className={`m-bar ${isOpenMenu}`} onClick={this.openMenu}></span>
                </div>

            ) : (
                <Link
                    to={!value.actionable ? `/${value.link}` : `#${value.link}`}
                    onClick={value.actionable ? (e) => { this.changeCategory(e,value.link) } : () => this.setState({isOpenMenu: false}) }>
                    { value.title }
                </Link>
            );
            return(
                value.active ?
                    <li key={index} className={`item ${isOpenMenu}`} id={`ìtem_${index}`}>
                        {type}
                    </li> : null
            );

        });
        return(
            <div className={`items-menu-content ${isOpenMenu}`} style={{
                height: isOpenMenu ? this.state.topHeightNav : '',
            }}>
                <ul className={`items-menu`}  ref={(e) => this.ofHeight = e}>

                    {menuItems}
                    <li className={`item search ${isOpenMenu}`}>
                        <Search
                            onSubmit={this.onSubmitSearch}
                            animation={false}
                            value={value}
                            placeholder={'Buscar'}/>
                    </li>
                </ul>
            </div>
        );
    }

    changeCategory(e,value) {
        const {
            dispatch,
            location: {
                pathname
            }
        } = this.props;
        if (!pathname.includes('search')) {
            if (value === 'cursos' || value === 'tutoriales') {
                this.scroll(e, value);
                return
            }
        } else {
            window.location.href = `/#${value}`;
        }
        dispatch(setTypeCategory(value));
    }
}
