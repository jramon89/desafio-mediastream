import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Image from './Image';
import Title from './Title';

class Container extends Component {
	static propTypes = {
		poster_path: PropTypes.string,
		title: PropTypes.string.isRequired,
		backdrop_path: PropTypes.string,
		original_title: PropTypes.string,
		id: PropTypes.number.isRequired
	}

	render() {
		const {
			poster_path,
			backdrop_path,
			original_title,
			id,
			title
		} = this.props;

		return(
			<div className="category-content-elements">
				{ /* <Image image={image}/> */ }
				<div className={`reference-graphic`}>
					<div className='fav-icon'>
						<a href="#" title="Agregar a fovoritos">
							<span className="fa fa-heart"></span>
						</a>
					</div>
					<img src={`https://image.tmdb.org/t/p/w500/${poster_path}`} alt=""/>
				</div>
				<Title
					title={title}
					category={'Gen'}
					id={id}/>

			</div>
		);
	}
} 
export default Container;