import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default
class Title extends Component {
	static propTypes = {
		title: PropTypes.string,
		id: PropTypes.number

	}
	
	seoUrl() {
		const {
			title,
		} = this.props;
		const cleanUrl = title.replace(/\s/g,"-").toLowerCase();

		return `movie/${cleanUrl}`;
	}
	getDetailsCat() {
		const { id } = this.props;
		localStorage.setItem(
			'itemSelected',
			JSON.stringify({
				id: id
			})
		)
	}
	render() {
		const { title, pathname } = this.props;

		return(
			<div className="category-title">
				<p><Link to={this.seoUrl()} onClick={this.getDetailsCat.bind(this)}>{title}</Link></p>
			</div>
		);
	}
}
