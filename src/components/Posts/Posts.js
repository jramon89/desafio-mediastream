import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Container from './Commons/Container.js';

class Posts extends Component {
	static propTypes = {
		data: PropTypes.array,
	};

	componentWillReceiveProps(nextProps, nextContext) {
		const {
			meta:{
				title,
				description
			}
		} = nextProps;
		document.title = title;
		document.all.description.content = description;
	}

	render() {
		const {
			data,
			style
		} = this.props;
		const object = data ? data : [];
		const list = object.map((value, index) => {
			return (
				<div className="cotegory-content-item" key={index}>
					<Container
						poster_path={ value.poster_path }
						title={ value.title }
						backdrop_path={ value.backdrop_path }
						original_title={ value.original_title }
						id={value.id}/>

				</div>
			);
		});
		return(
			<div className={`category-content ${style}`}>
				{ list }
			</div>
		);
	}
}
export default Posts;