import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default
class DetailsCategoriesList extends Component {
	static propTypes = {
		details: PropTypes.object
	};
	constructor(props){
		super(props);
		this.state={
			items:[]
		};
	}
	componentWillReceiveProps(nextProps) {
        document.title = nextProps.details.title;
        document.all.description.content = nextProps.details.title;
	}

	render() {
		const { details } = this.props;
		console.log('details', details);
		return(
			<div className="details-category-content">
				<div className="details-title">{details.original_title}</div>
				<div className="poster">
					<img src={`https://image.tmdb.org/t/p/w500/${details.poster_path}`} alt=""/>
				</div>
				<div className="overview">
					{ details.overview }
				</div>
				<div className='overview-img'>
					<img src={`https://image.tmdb.org/t/p/w500/${details.backdrop_path}`} alt=""/>
				</div>
			</div>
		);
	}
}
