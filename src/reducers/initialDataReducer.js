import {
    GET_INITIAL_DATA,
    GET_INITIAL_COURSES,
    GET_INITIAL_TUTORIALS,
    GET_ALL_COURSES_SUCCESS,
    GET_ALL_TUTORIALS_SUCCESS
} from "../actions/actionsTypes";


const initialState = {
    posts: [{
        "vote_count": 20,
        "id": 280960,
        "video": false,
        "vote_average": 5.2,
        "title": "",
        "popularity": 264.211,
        "poster_path": "",
        "original_language": "",
        "original_title": "",
        "genre_ids": [
            18,
            9648
        ],
        "backdrop_path": "",
        "adult": false,
        "overview": "",
        "release_date": ""
    }],
    courses: [],
    tutorials: [],
};

export default function(state=initialState, action){
    switch(action.type){
        case GET_INITIAL_DATA:
            return {
                ...state,
                posts: action.data,
            };
        case GET_ALL_COURSES_SUCCESS:
            return {
                ...state,
                courses: action.data
            };
        case GET_ALL_TUTORIALS_SUCCESS:
            return {
                ...state,
                tutorials: action.data
            };
        default:
            return state

    }
}