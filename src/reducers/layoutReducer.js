import { GET_INIT_VALUES  } from "../actions/actionsTypes";


const initialState = {
	menu:{
		menu_a: [
			{
				"title": "Home",
				"link": "/",
				"active": true,
				"actionable": false,
				"lang_id": "APP"
			},
			{
				"title": "Todas",
				"link": "",
				"active": true,
				"actionable": false,
				"lang_id": "APP_MENU_COURSES"
			},
			{
				"title": "Más populares",
				"link": "populares",
				"active": true,
				"actionable": false,
				"lang_id": "APP_MENU_TUTORIALS"
			},
		],
		menu_b: []
	}
};

export default function(state=initialState, action){
	switch(action.type){
		
		case GET_INIT_VALUES:
			return {
				menu: state.menu
			};
		default:
			return state

	}
}