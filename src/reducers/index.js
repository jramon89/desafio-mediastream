import { combineReducers } from 'redux';
import layout from './layoutReducer';
import events from './eventsReducer';
import initialData from './initialDataReducer';

const reducers = combineReducers({
	layout,
	events,
	initialData
});

export default reducers;