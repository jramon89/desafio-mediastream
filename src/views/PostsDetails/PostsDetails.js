import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PostsListDetails from "../../components/PostsListDetails/PostsListDetails";
import { hideLayoutElements  } from '../../actions/actionsLayout';


@connect(state=>({
    posts: state.initialData.posts.results,
}))

class PostsDetails extends Component{
	
	static propTypes = {
		data:PropTypes.object,
		all: PropTypes.array,
        posts: PropTypes.array
	};

	constructor(props) {
		super(props);
		this.state = {
			details: {}
		}
	}

	componentDidMount() {
		const {
			dispatch,
            location: {
                pathname
            },
			posts,
		} = this.props;

        /*
		if (localStorage.getItem('itemSelected')) {
            this.props.dispatch(fetchDetailsPost(
                JSON.parse(localStorage.getItem('itemSelected'))
            ));
        }
        */
        dispatch(hideLayoutElements(true));
        const path = pathname.split('/');

	}

	componentWillUnmount() {
		const { dispatch } = this.props;
		dispatch(hideLayoutElements(false));
	}

	render() {
		const {
			details,
			posts
		} = this.props;

		const info = posts.find((value) => {
			return value.id === JSON.parse(localStorage.getItem('itemSelected')).id;
		});

		return(
			<div>
				<PostsListDetails
				details={ info }/>
			</div>
		);
	}
}

export default PostsDetails;
