import Layout from './Layout/Layout';
import Home from './Home/Home';
import PostsDetails from './PostsDetails/PostsDetails';
import Popular from './Populares/Populares';
import Search from './Search/Search'
export default {
	Layout,
	Home,
	PostsDetails,
	Popular,
	Search,
}