import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import HomeComponent from '../../components/Home/Home'

@connect(state=>({
    posts: state.initialData.posts,
}))

class Home extends Component {
    static propTypes = {};

    constructor(props){
        super(props);
    }



    render() {
        const {
            posts
        } = this.props;
        return(
            <div>
               <HomeComponent posts={posts.results}/>
            </div>
        );
    }
}

export default Home;