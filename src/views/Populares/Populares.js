import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PopularList from '../../components/populares/Pupulares';
import {connect} from "react-redux";

@connect(state=>({
    posts: state.initialData.posts,
}))
export default
class Popular extends Component {
    static propTypes = {
        posts: PropTypes.object
    };
    constructor(props){
        super(props);
        this.state={
            items:[]
        };
    }
    componentWillReceiveProps(nextProps) {
        document.title = 'Peliculas mas populares';
        document.all.description.content = 'Peliculas mas populares';
    }

    render() {
        return(
            <div>
                <PopularList data={ this.props.posts.results }/>
            </div>
        );
    }
}
